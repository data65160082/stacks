/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.stack;

/**
 *
 * @author Tauru
 */
class StackApp {

    public static void main(String[] args) {
        StackX theStack = new StackX(10); // Create a new stack
        theStack.push(20); // Push items onto the stack
        theStack.push(40);
        theStack.push(60);
        theStack.push(80);

        while (!theStack.isEmpty()) {
            long value = theStack.pop(); // Delete item from the stack
            System.out.print(value + " "); // Display it
        }

        System.out.println();
    }
}

